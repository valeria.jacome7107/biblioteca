<?php

/**
 *
 */
class Socios extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Socio");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoSocios"]=
                    $this->Socio->consultarTodos();
    $this->load->view("header");
    $this->load->view("socios/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id){
    $this->Socio->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Socio eliminado existosamente");
    redirect('socios/index');
}



  //Renderizando hospitales
  public function nuevo(){
      $this->load->view("header");
      $this->load->view("socios/index", $data);
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){
      $this->load->model("Socio");
      $data["socioEditar"] = $this->Socio->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("socios/editar", $data);
      $this->load->view("footer");
  }



  //insertar hospitales
  public function guardarSocio(){
    $datosNuevosSocio=array(
      "cedula" => $this->input->post("cedula"),
      "nombre" => $this->input->post("nombre"),
      "direccion" => $this->input->post("direccion"),
      "telefono" => $this->input->post("telefono"),
      "correo" => $this->input->post("correo"),
      "fecha_inscripcion" => $this->input->post("fecha_inscripcion")
    );
    $this->Socio->insertar($datosNuevosSocio);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","socio guardado exitosamente");
      redirect('socios/index');
  }

  public function actualizarSocio()
{
 $id = $this->input->post("id");

 // Obtener los datos actuales de la agencia
 $socioEditar = $this->Socio->obtenerPorId($id);

 // Obtener los datos actualizados del formulario
 $datosSocio = array(
   "cedula" => $this->input->post("cedula"),
   "nombre" => $this->input->post("nombre"),
   "direccion" => $this->input->post("direccion"),
   "telefono" => $this->input->post("telefono"),
   "correo" => $this->input->post("correo"),
   "fecha_inscripcion" => $this->input->post("fecha_inscripcion")
 );


 // Actualizar la agencia con los nuevos datos
 $this->Socio->actualizar($id, $datosSocio);

 // Flash message
 $this->session->set_flashdata("confirmacion", "Socio actualizado exitosamente");

 // Redireccionar a la página de lista de agencias
 redirect('socios/index');
}





}



 ?>
