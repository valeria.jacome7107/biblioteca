<?php

/**
 *
 */
class Devoluciones extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Devolucion");
    error_reporting(0);

  }
  public function index(){
    // Utiliza una sola función de consulta que traiga la información completa
        $data["listadoDevoluciones"] = $this->Devolucion->consultarTodosConPrestamoPersonal();

        // Carga los listados de socios, libros y personal
        $data["listadoPrestamos"] = $this->Devolucion->obtenerListadoPrestamos();
        $data["listadoPersonales"] = $this->Devolucion->obtenerListadoPersonales();

        $this->load->view("header");
        $this->load->view("devoluciones/index", $data);
        $this->load->view("footer");
    }

  public function borrar($id){
    $this->Devolucion->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Devolución eliminada existosamente");
    redirect('devoluciones/index');
}

public function guardarDevolucion(){
    $datosNuevosDevolucion = array(
      "fecha_devolucion" => $this->input->post("fecha_devolucion"),
      "estado" => $this->input->post("estado"),
      "fkid_personal" => $this->input->post("fkid_personal"),
      "fkid_prestamo" => $this->input->post("fkid_prestamo")

    );

    $this->Devolucion->insertar($datosNuevosDevolucion);

    // Flash message
    $this->session->set_flashdata("confirmacion","Devolución guardado exitosamente");

    redirect('devoluciones/index');
}



  public function nuevo(){
      $this->load->view("header");
      $this->load->view("devoluciones/nuevo");
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){

      $data["listadoPrestamos"] = $this->Devolucion->obtenerListadoPrestamos();
      $data["listadoPersonales"] = $this->Devolucion->obtenerListadoPersonales();
      $data["devolucionEditar"] = $this->Devolucion->obtenerPorId($id);

      $this->load->view("header");
      $this->load->view("devoluciones/editar", $data);
      $this->load->view("footer");
  }

#aqui me quede ----------------------------------------------------------

public function actualizarDevolucion(){
    $id = $this->input->post("id");

    // Obtener los datos actuales de la autoría
    $devolucionEditar = $this->Devolucion->obtenerPorId($id);

    // Obtener los datos actualizados del formulario
    $datosDevolucion = array(
      "fecha_devolucion" => $this->input->post("fecha_devolucion"),
      "estado" => $this->input->post("estado"),
      "fkid_personal" => $this->input->post("fkid_personal"),
      "fkid_prestamo" => $this->input->post("fkid_prestamo")
    );

    // Actualizar los datos de la autoría en la base de datos
    $this->Devolucion->actualizar($id, $datosDevolucion);

    // Flash message
    $this->session->set_flashdata("confirmacion", "Devolución actualizada exitosamente");

    // Redireccionar a la página de lista de revistas
    redirect('devoluciones/index');
}






}











 ?>
