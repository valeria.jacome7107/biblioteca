<?php

/**
 *
 */
class Multas extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Multa");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoMultas"]=
                    $this->Multa->consultarTodos();
    $data["listadoMultas"] = $this->Multa->consultarTodosConDevoluciones();
      $data["listadoDevoluciones"] = $this->Multa->obtenerListadoDevoluciones();
    $this->load->view("header");
    $this->load->view("multas/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id){
    $this->Multa->eliminar($id);
    $this->session->set_flashdata("confirmacion", "multa eliminado existosamente");
    redirect('multas/index');
}



  //Renderizando hospitales
  public function nuevo(){
      $data["listadoDevoluciones"] = $this->Multa->obtenerListadoDevoluciones();
      $this->load->view("header");
      $this->load->view("multas/index", $data);
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){
      $data["listadoDevoluciones"] = $this->Multa->obtenerListadoDevoluciones();
      $data["multaEditar"] = $this->Multa->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("multas/editar", $data);
      $this->load->view("footer");
  }



  //insertar hospitales
  public function guardarMulta(){
    $datosNuevosMulta=array(
      "monto" => $this->input->post("monto"),
      "pagado" => $this->input->post("pagado"),
      "fecha_pago" => $this->input->post("fecha_pago"),
      "fkid_devolucion" => $this->input->post("fkid_devolucion")
    );
    $this->Multa->insertar($datosNuevosMulta);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","Multa guardado exitosamente");
      redirect('multas/index');
  }

  public function actualizarMulta()
{
 $id = $this->input->post("id");

 // Obtener los datos actuales de la agencia
 $multaEditar = $this->Multa->obtenerPorId($id);

 // Obtener los datos actualizados del formulario
 $datosMulta = array(
   "monto" => $this->input->post("monto"),
   "pagado" => $this->input->post("pagado"),
   "fecha_pago" => $this->input->post("fecha_pago"),
   "fkid_devolucion" => $this->input->post("fkid_devolucion")
 );

 // Actualizar la agencia con los nuevos datos
 $this->Multa->actualizar($id, $datosMulta);

 // Flash message
 $this->session->set_flashdata("confirmacion", "Multa actualizado exitosamente");

 // Redireccionar a la página de lista de agencias
 redirect('multas/index');
}





}



 ?>
