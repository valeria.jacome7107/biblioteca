<?php

/**
 *
 */
class Prestamos extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model("Prestamo");
    error_reporting(0);

  }
  public function index(){
    // Utiliza una sola función de consulta que traiga la información completa
        $data["listadoPrestamos"] = $this->Prestamo->consultarTodosConLibroSocioPersonal();

        // Carga los listados de socios, libros y personal
        $data["listadoSocios"] = $this->Prestamo->obtenerListadoSocios();
        $data["listadoLibros"] = $this->Prestamo->obtenerListadoLibros();
        $data["listadoPersonales"] = $this->Prestamo->obtenerListadoPersonales();

        $this->load->view("header");
        $this->load->view("prestamos/index", $data);
        $this->load->view("footer");
    }

  public function borrar($id){
    $this->Prestamo->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Préstamo eliminada existosamente");
    redirect('prestamos/index');
}

public function guardarPrestamo(){
    $datosNuevosPrestamo = array(
      "fecha_prestamo" => $this->input->post("fecha_prestamo"),
      "fecha_devolucion_prevista" => $this->input->post("fecha_devolucion_prevista"),
      "fkid_personal" => $this->input->post("fkid_personal"),
      "fkid_libro" => $this->input->post("fkid_libro"),
      "fkid_socio" => $this->input->post("fkid_socio")
    );

    $this->Prestamo->insertar($datosNuevosPrestamo);

    // Flash message
    $this->session->set_flashdata("confirmacion","Préstamo guardado exitosamente");

    redirect('prestamos/index');
}



  public function nuevo(){
      $this->load->view("header");
      $this->load->view("prestamos/nuevo");
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){

      $data["listadoSocios"] = $this->Prestamo->obtenerListadoSocios();
      $data["listadoLibros"] = $this->Prestamo->obtenerListadoLibros();
      $data["listadoPersonales"] = $this->Prestamo->obtenerListadoPersonales();
      $data["prestamoEditar"] = $this->Prestamo->obtenerPorId($id);

      $this->load->view("header");
      $this->load->view("prestamos/editar", $data);
      $this->load->view("footer");
  }

#aqui me quede ----------------------------------------------------------

public function actualizarPrestamo(){
    $id = $this->input->post("id");

    // Obtener los datos actuales de la autoría
    $prestamoEditar = $this->Prestamo->obtenerPorId($id);

    // Obtener los datos actualizados del formulario
    $datosPrestamo = array(
      "fecha_prestamo" => $this->input->post("fecha_prestamo"),
      "fecha_devolucion_prevista" => $this->input->post("fecha_devolucion_prevista"),
      "fkid_personal" => $this->input->post("fkid_personal"),
      "fkid_libro" => $this->input->post("fkid_libro"),
      "fkid_socio" => $this->input->post("fkid_socio")
    );

    // Actualizar los datos de la autoría en la base de datos
    $this->Prestamo->actualizar($id, $datosPrestamo);

    // Flash message
    $this->session->set_flashdata("confirmacion", "Préstamo actualizada exitosamente");

    // Redireccionar a la página de lista de revistas
    redirect('prestamos/index');
}

public function generatePdf($id) {
    // Cargar los modelos necesarios
    $this->load->model('Prestamo');
    $this->load->model('Libro');
    $this->load->model('Personal');
    $this->load->model('Socio');

    // Obtener los datos del préstamo por su ID
    $prestamo = $this->Prestamo->obtenerPorId($id);

    // Obtener los datos del libro, socio y personal relacionado al préstamo
    $libro = $this->Libro->obtenerPorId($prestamo->fkid_libro);
    $socio = $this->Socio->obtenerPorId($prestamo->fkid_socio);
    $personal = $this->Personal->obtenerPorId($prestamo->fkid_personal);

    // Preparar el contenido para el PDF
    $pdfContent = [
        ['text' => 'Biblioteca Municipal Eugenio Espejo', 'style' => 'header'],
        ['text' => 'Préstamo de Libros', 'style' => 'subheader'],
        ['text' => 'Datos del Libro', 'style' => 'section'],
        ['text' => "Nombre del libro: {$libro->nombre}"],
        ['text' => "Autor: {$libro->autor}"],
        ['text' => "ISBN: {$libro->isbn}"],
        ['text' => "Fecha de préstamo: {$prestamo->fecha_prestamo}"],
        ['text' => "Fecha de Devolución: {$prestamo->fecha_devolucion_prevista}"],
        ['text' => 'Datos Personales del Socio', 'style' => 'section'],
        ['text' => "Nombre: {$socio->nombre}"],
        ['text' => "Dirección: {$socio->direccion}"],
        ['text' => "Teléfono: {$socio->telefono}"],
        ['text' => 'Datos del bibliotecario', 'style' => 'section'],
        ['text' => "Nombre de quien entrega: {$personal->nombre}"],
        ['text' => "Puesto: {$personal->puesto}"],
        ['text' => "Teléfono: {$personal->telefono}"]
    ];

    // Pasar el contenido a la vista
    $this->load->view('pdf_view', ['pdfContent' => $pdfContent]);
}


}











 ?>
