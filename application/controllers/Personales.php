<?php

/**
 *
 */
class Personales extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Personal");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoPersonales"]=
                    $this->Personal->consultarTodos();
    $this->load->view("header");
    $this->load->view("personales/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id){
    $this->Personal->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Personal eliminado existosamente");
    redirect('personales/index');
}



  //Renderizando hospitales
  public function nuevo(){
      $this->load->view("header");
      $this->load->view("personales/index", $data);
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){
      $this->load->model("Personal");
      $data["personalEditar"] = $this->Personal->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("personales/editar", $data);
      $this->load->view("footer");
  }



  //insertar hospitales
  public function guardarPersonal(){
        /*INICIO PROCESO DE SUBIDA DE ARCHIVO*/
      $config['upload_path']=APPPATH.'../uploads/personal/'; //ruta de subida de archivos
      $config['allowed_types']='jpeg|jpg|png';//tipo de archivos permitidos
      $config['max_size']=5*1024;//definir el peso maximo de subida (5MB)
      $nombre_aleatorio="firma_".time()*rand(100,10000);//creando un nombre aleatorio
      $config['file_name']=$nombre_aleatorio;//asignando el nombre al archivo subido
      $this->load->library('upload',$config);//cargando la libreria UPLOAD
      if($this->upload->do_upload("fotografia")){ //intentando subir el archivo
         $dataArchivoSubido=$this->upload->data();//capturando informacion del archivo subido
         $nombre_archivo_subido=$dataArchivoSubido["file_name"];//obteniendo el nombre del archivo
      }else{
        $nombre_archivo_subido="";//Cuando no se sube el archivo el nombre queda VACIO
      }
    $datosNuevosPersonal=array(
      "nombre" => $this->input->post("nombre"),
      "direccion" => $this->input->post("direccion"),
      "puesto" => $this->input->post("puesto"),
      "telefono" => $this->input->post("telefono"),
      "fecha_contratacion" => $this->input->post("fecha_contratacion"),
      "estado" => $this->input->post("estado"),
      "fotografia" => $nombre_archivo_subido
    );
    $this->Personal->insertar($datosNuevosPersonal);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","empleado guardado exitosamente");
      redirect('personales/index');
  }

  public function actualizarPersonal()
{
 $id = $this->input->post("id");

 // Obtener los datos actuales de la agencia
 $personalEditar = $this->Personal->obtenerPorId($id);

 // Obtener los datos actualizados del formulario
 $datosPersonal = array(
   "nombre" => $this->input->post("nombre"),
   "direccion" => $this->input->post("direccion"),
   "puesto" => $this->input->post("puesto"),
   "telefono" => $this->input->post("telefono"),
   "fecha_contratacion" => $this->input->post("fecha_contratacion"),
   "estado" => $this->input->post("estado")
 );
 if (!empty($_FILES['fotografia_nueva']['name'])) {
     $config['upload_path'] = APPPATH . '../uploads/personal/';
     $config['allowed_types'] = 'jpeg|jpg|png';
     $config['max_size'] = 5 * 1024; // 5 MB

     $nombre_aleatorio = "fotografia_" . time() * rand(100, 10000);
     $config['file_name'] = $nombre_aleatorio;

     $this->load->library('upload', $config);

     if ($this->upload->do_upload("fotografia_nueva")) {
         $dataArchivoSubido = $this->upload->data();
         $datosPersonal["fotografia"] = $dataArchivoSubido["file_name"];
     } else {
         $error = $this->upload->display_errors();
         return;
     }
 } else {
     // Si no se proporciona una nueva imagen, mantener la imagen actual
     $datosPersonal["fotografia"] = $personalEditar->fotografia;
 }


 // Actualizar la agencia con los nuevos datos
 $this->Personal->actualizar($id, $datosPersonal);

 // Flash message
 $this->session->set_flashdata("confirmacion", "Personal actualizado exitosamente");

 // Redireccionar a la página de lista de agencias
 redirect('personales/index');
}





}



 ?>
