<?php

/**
 *
 */
class Libros extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Libro");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoLibros"]=
                    $this->Libro->consultarTodos();
    $this->load->view("header");
    $this->load->view("libros/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id){
    $this->Libro->eliminar($id);
    $this->session->set_flashdata("confirmacion", "Libro eliminado existosamente");
    redirect('libros/index');
}



  //Renderizando hospitales
  public function nuevo(){
      $this->load->view("header");
      $this->load->view("libros/index", $data);
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id){
      $this->load->model("Libro");
      $data["libroEditar"] = $this->Libro->obtenerPorId($id);
      $this->load->view("header");
      $this->load->view("libros/editar", $data);
      $this->load->view("footer");
  }



  //insertar hospitales
  public function guardarLibro(){
    $datosNuevosLibro=array(
      "nombre" => $this->input->post("nombre"),
      "autor" => $this->input->post("autor"),
      "isbn" => $this->input->post("isbn"),
      "editorial" => $this->input->post("editorial"),
      "idioma" => $this->input->post("idioma"),
      "num_ejemplares" => $this->input->post("num_ejemplares")
    );
    $this->Libro->insertar($datosNuevosLibro);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","libro guardado exitosamente");
      redirect('libros/index');
  }

  public function actualizarLibro()
{
 $id = $this->input->post("id");

 // Obtener los datos actuales de la agencia
 $libroEditar = $this->Libro->obtenerPorId($id);

 // Obtener los datos actualizados del formulario
 $datosLibro = array(
   "nombre" => $this->input->post("nombre"),
   "autor" => $this->input->post("autor"),
   "isbn" => $this->input->post("isbn"),
   "editorial" => $this->input->post("editorial"),
   "idioma" => $this->input->post("idioma"),
   "num_ejemplares" => $this->input->post("num_ejemplares")
 );


 // Actualizar la agencia con los nuevos datos
 $this->Libro->actualizar($id, $datosLibro);

 // Flash message
 $this->session->set_flashdata("confirmacion", "Libro actualizado exitosamente");

 // Redireccionar a la página de lista de agencias
 redirect('libros/index');
}





}



 ?>
