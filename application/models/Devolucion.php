<?php
  class Devolucion extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("devolucion",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $devoluciones=$this->db->get("devolucion");
      if ($devoluciones->num_rows()>0) {
        return $devoluciones->result();
      } else {
        return false;
      }
    }

    public function consultarTodosConPrestamoPersonal() {
    $this->db->select('devolucion.*, prestamo.fecha_devolucion_prevista AS fecha_entrega, personal.nombre AS nombre_personal');
    $this->db->from('devolucion');
    $this->db->join('prestamo', 'devolucion.fkid_prestamo = prestamo.id', 'left');
    $this->db->join('personal', 'devolucion.fkid_personal = personal.id', 'left');
    $query = $this->db->get();
    return $query->result();
}


    // Obtener hospital por ID
function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $devolucion = $this->db->get("devolucion");
    if ($devolucion->num_rows() > 0) {
        return $devolucion->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("devolucion");
    }

    //funcion para actualizar hospitales
function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("devolucion",$datos);
}




function obtenerListadoPrestamos()
{
    $prestamos = $this->db->get("prestamo")->result();
    return $prestamos;
}


   function obtenerListadoPersonales()
   {
       $personales = $this->db->get("personal")->result();
       return $personales;
   }



  }//Fin de la clase



?>
