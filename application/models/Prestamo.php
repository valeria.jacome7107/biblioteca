<?php
  class Prestamo extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("prestamo",$datos);
      return $respuesta;
    }
    //Consulta de datos
    function consultarTodos(){
      $prestamos=$this->db->get("prestamo");
      if ($prestamos->num_rows()>0) {
        return $prestamos->result();
      } else {
        return false;
      }
    }

    public function consultarTodosConLibroSocioPersonal() {
    $this->db->select('prestamo.*, libro.nombre AS nombre_libro, socio.nombre AS nombre_socio, personal.nombre AS nombre_personal');
    $this->db->from('prestamo');
    $this->db->join('libro', 'prestamo.fkid_libro = libro.id', 'left');
    $this->db->join('socio', 'prestamo.fkid_socio = socio.id', 'left');
    $this->db->join('personal', 'prestamo.fkid_personal = personal.id', 'left');
    $query = $this->db->get();
    return $query->result();
}


    public function consultarTodosConPersonales()
    {
        $this->db->select('prestamo.*, personal.nombre AS nombre_personal');
        $this->db->from('prestamo');
        $this->db->join('personal', 'prestamo.fkid_personal = personal.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    // Consulta de todos los préstamos con el nombre del libro
    public function consultarTodosConLibros()
    {
        $this->db->select('prestamo.*, libro.nombre AS nombre_libro');
        $this->db->from('prestamo');
        $this->db->join('libro', 'prestamo.fkid_libro = libro.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    // Consulta de todos los préstamos con el nombre del socio
    public function consultarTodosConSocios()
    {
        $this->db->select('prestamo.*, socio.nombre AS nombre_socio');
        $this->db->from('prestamo');
        $this->db->join('socio', 'prestamo.fkid_socio = socio.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    // Obtener hospital por ID
function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $prestamo = $this->db->get("prestamo");
    if ($prestamo->num_rows() > 0) {
        return $prestamo->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("prestamo");
    }

    //funcion para actualizar hospitales
function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("prestamo",$datos);
}




function obtenerListadoSocios()
{
    $socios = $this->db->get("socio")->result();
    return $socios;
}



   function obtenerListadoLibros()
   {
       $libros = $this->db->get("libro")->result();
       return $libros;
   }

   function obtenerListadoPersonales()
   {
       $personales = $this->db->get("personal")->result();
       return $personales;
   }



  }//Fin de la clase



?>
