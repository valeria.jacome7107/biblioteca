<?php
    class Personal extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("personal");

      return $respuesta;
  }


    //Consulta de datos
    function consultarTodos(){
      $personales=$this->db->get("personal");
      if ($personales->num_rows()>0) {
        return $personales->result();
      } else {
        return false;
      }
    }


function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $personal = $this->db->get("personal");
    if ($personal->num_rows() > 0) {
        return $personal->row();
    } else {
        return false;
    }
}



    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("personal");
    }

function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("personal",$datos);
}


  }//Fin de la clase
?>
