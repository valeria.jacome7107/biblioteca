<?php
    class Multa extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales

    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("multa");

      return $respuesta;
  }
    //Consulta de datos
    function consultarTodos(){
      $multas=$this->db->get("multa");
      if ($multas->num_rows()>0) {
        return $multas->result();
      } else {
        return false;
      }
    }

    public function consultarTodosConDevoluciones() {
        $this->db->select('multa.*, devolucion.id AS id_devolucion');
        $this->db->from('multa');
        $this->db->join('devolucion', 'multa.fkid_devolucion = devolucion.id', 'left');
        $query = $this->db->get();
        return $query->result();
    }




    // Obtener hospital por ID
function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $multa = $this->db->get("multa");
    if ($multa->num_rows() > 0) {
        return $multa->row();
    } else {
        return false;
    }
}



    //eliminacion de hospital por id
    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("multa");
    }

    //funcion para actualizar hospitales
function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("multa",$datos);
}

function obtenerListadoDevoluciones()
   {
       $devoluciones = $this->db->get("devolucion")->result();
       return $devoluciones;
   }

  }//Fin de la clase
?>
