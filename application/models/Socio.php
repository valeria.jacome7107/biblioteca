<?php
    class Socio extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("socio");

      return $respuesta;
  }


    //Consulta de datos
    function consultarTodos(){
      $socios=$this->db->get("socio");
      if ($socios->num_rows()>0) {
        return $socios->result();
      } else {
        return false;
      }
    }


function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $socio = $this->db->get("socio");
    if ($socio->num_rows() > 0) {
        return $socio->row();
    } else {
        return false;
    }
}



    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("socio");
    }

function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("socio",$datos);
}


  }//Fin de la clase
?>
