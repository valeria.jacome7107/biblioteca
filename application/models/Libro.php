<?php
    class Libro extends CI_Model
  {
    function __construct()
    {
      parent::__construct();
    }
    //Insertar nuevos hospitales
    function insertar($datos){
      // Utiliza el método set() para establecer los datos que deseas insertar
      $this->db->set($datos);

      // Luego, realiza la inserción en la tabla 'editorial'
      $respuesta = $this->db->insert("libro");

      return $respuesta;
  }


    //Consulta de datos
    function consultarTodos(){
      $libros=$this->db->get("libro");
      if ($libros->num_rows()>0) {
        return $libros->result();
      } else {
        return false;
      }
    }


function obtenerPorId($id)
{
    $this->db->where("id", $id);
    $libro = $this->db->get("libro");
    if ($libro->num_rows() > 0) {
        return $libro->row();
    } else {
        return false;
    }
}



    function eliminar($id){
        $this->db->where("id",$id);
        return $this->db->delete("libro");
    }

function actualizar($id,$datos){
  $this->db->where("id",$id);
  return $this->db->update("libro",$datos);
}


  }//Fin de la clase
?>
