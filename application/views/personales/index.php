<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
         <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;PERSONAL</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar nuevo Personal
      </button>


    </div>

  </div><br>


  <?php if ($listadoPersonales): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>DIRECCIÓN</th>
            <th>PUESTO</th>
            <th>TELEFONO</th>
            <th>FECHA DE CONTRATACIÓN</th>
            <th>ESTADO</th>
            <th>FOTO</th>
            <th>ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoPersonales as $personal): ?>
            <tr>
                <td class="text-dark"><?php echo $personal->id; ?></td>
                <td class="text-dark"><?php echo $personal->nombre; ?></td>
                <td class="text-dark"><?php echo $personal->direccion; ?></td>
                <td class="text-dark"><?php echo $personal->puesto; ?></td>
                <td class="text-dark"><?php echo $personal->telefono; ?></td>
                <td class="text-dark"><?php echo $personal->fecha_contratacion; ?></td>
                <td class="text-dark"><?php echo $personal->estado; ?></td>
                <td>
                      <?php if ($personal->fotografia!=""): ?>
                        <img src="<?php echo base_url('uploads/personal/').$personal->fotografia; ?>"
                        height="75px" alt="">
                      <?php else: ?>
                        N/A
                      <?php endif; ?>
                </td>
                <td>
                    <a href="<?php echo site_url('personales/editar/').$personal->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('personales/borrar/').$personal->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro personal registrado
          </div>
  <?php endif; ?>
</div>


      <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"> Nuevo Personal
                  </h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
      <div class="container">
          <form  class="text-dark" action="<?php echo site_url('personales/guardarPersonal') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_personal">

            <div class="mb-3 text-dark">
                <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
                <input id="nombre" type="text" name="nombre" value="" oninput="validarLetras(this)" placeholder="Ingrese el nombre del empleado" class="form-control" required>
            </div>
              <div class="mb-3 text-dark">
                  <label for="direccion" class="form-label text-dark"><b>Dirección:</b></label>
                  <input id="direccion" type="text" name="direccion" value="" oninput="" placeholder="Ingrese al dirección" class="form-control" required>
              </div>
              <div class="mb-3 text-dark">
                  <label for="puesto" class="form-label text-dark"><b>Puesto:</b></label>
                  <input id="puesto" type="text" name="puesto" value="" oninput="validarLetras(this)" placeholder="Ingrese la puesto del empleado" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="telefono" class="form-label"><b>Teléfono:</b></label>
                  <input id="telefono" type="text" name="telefono" value="" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="fecha_contratacion" class="form-label"><b>Fecha de contración:</b></label>
                  <input id="fecha_contratacion" type="date" name="fecha_contratacion" value="" oninput="" placeholder="Ingrese la fecha de contratación" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="estado" class="form-label"><b>Estado:</b></label>
                  <select id="estado" name="estado" class="form-control" required>
                      <option value="">Seleccione el estado</option>
                      <option value="Activo">Activo</option>
                      <option value="Inactivo">Inactivo</option>
                  </select>
              </div>
              <div class="mb-3">
                <label for="fotografia"> <b>Foto: </b> </label>
                <input type="file" name="fotografia" id="fotografia" value="" class="form-control" required accept="image/*"><br>
              </div>

              <div class="row justify-content-end">
                  <div class="col-auto">
                      <button type="submit" name="button" class="btn btn-success">
                          <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                      </button>
                  </div>
                  <div class="col-auto">
                      <a class="btn btn-danger" href="<?php echo site_url('personales/index') ?>">
                          <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                      </a>
                  </div>
              </div>
          </form>
      </div>
  </div>

  </div>
  </div>
    </div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>


      <script type="text/javascript">
      $(document).ready(function() {
        $.validator.addMethod("fechaActual", function(value, element) {
          var fechaActual = new Date();
          var fechaInscripcion = new Date(value);
          return fechaInscripcion <= fechaActual;
        }, "La fecha de contratación no puede ser después de la fecha actual.");
          $("#frm_nuevo_personal").validate({
              rules: {
                "nombre": {
                    required: true,
                    minlength: 3,
                    maxlength: 50

                },
                "dirección": {
                    required: true,
                    minlength: 3,
                    maxlength: 150

                },
                  "puesto": {
                      required: true,
                      minlength: 3,
                      maxlength: 50

                  },

                  "telefono": {
                    required: true,
                    minlength: 7,
                    maxlength: 10
                  },
                  "fecha_contratacion": {
                    required: true,
                    fechaActual: true

                  },

                  "estado": {
                    required: true
                  },
                  "fotografia": {
                    required: true
                  }

              },
              messages: {
                "nombre": {
                    required: "Ingrese el nombre del empleado",
                    minlength: "El nombre del empleado no debe tener menos de 3 caracteres",
                    maxlength: "El nombre del empleado no debe tener más de 50 caracteres"

                },
                "direccion": {
                    required: "Ingrese la direccion",
                    minlength: "La dirección no debe tener menos de 3 caracteres",
                    maxlength: "La dirección no debe tener más de 150 caracteres"

                },
                  "puesto": {
                      required: "Ingrese el puesto",
                      minlength: "El puesto no debe tener menos de 3 caracteres",
                      maxlength: "El puesto no debe tener más de 50 caracteres"

                  },
                  "telefono": {
                    required: "Debe ingresar el telefono",
                    minlength: "El telefono no debe tener menos de 7 numeros",
                    maxlength: "El teléfono no debe tener más de 10 numeros"


                  },
                  "fecha_contratacion": {
                    required: "Debe ingresar la fecha de contración"
                  },
                  "estado": {
                    required: "Escoja el estado"
                  },

                  "fotografia": {
                    required: "Debe ingresar la foto del empleado"
                  }


              }
          });
      });


      </script>

      <style media="screen">
        input{
          color: black !important;
        }
      </style>
