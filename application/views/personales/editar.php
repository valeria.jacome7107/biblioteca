<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR PERSONAL
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('personales/actualizarPersonal'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_personal">

        <input type="hidden" value="<?php echo $personalEditar->id; ?>" name="id" id="id">
        <div class="mb-3 text-dark">
            <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
            <input id="nombre" type="text" name="nombre" value="<?php echo $personalEditar->nombre; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre del empleado" class="form-control" required>
        </div>
          <div class="mb-3 text-dark">
              <label for="direccion" class="form-label text-dark"><b>Dirección:</b></label>
              <input id="direccion" type="text" name="direccion" value="<?php echo $personalEditar->direccion; ?>" oninput="" placeholder="Ingrese al dirección" class="form-control" required>
          </div>
          <div class="mb-3 text-dark">
              <label for="puesto" class="form-label text-dark"><b>Puesto:</b></label>
              <input id="puesto" type="text" name="puesto" value="<?php echo $personalEditar->puesto; ?>" oninput="validarLetras(this)" placeholder="Ingrese la puesto del empleado" class="form-control" required>
          </div>
          <div class="mb-3">
              <label for="telefono" class="form-label"><b>Teléfono:</b></label>
              <input id="telefono" type="text" name="telefono" value="<?php echo $personalEditar->telefono; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control" required>
          </div>
          <div class="mb-3">
              <label for="fecha_contratacion" class="form-label"><b>Fecha de contración:</b></label>
              <input id="fecha_contratacion" type="date" name="fecha_contratacion" value="<?php echo $personalEditar->fecha_contratacion; ?>" oninput="" placeholder="Ingrese la fecha de contratación" class="form-control" required>
          </div>
          <div class="mb-3">
             <label for="estado" class="form-label"><b>Estado:</b></label>
             <select id="estado" name="estado" class="form-control" required>
                 <option value="">Seleccione el estado</option>
                 <option value="Activo" <?php if ($personalEditar->estado == "Activo") echo "selected"; ?>>Activo</option>
                 <option value="Inactivo" <?php if ($personalEditar->estado == "Inactivo") echo "selected"; ?>>Inactivo</option>
             </select>
         </div>

          <div class="mb-3">
            <label for=""><b>Foto:</b></label><br>
              <?php if (!empty($personalEditar->fotografia)) : ?>
                  <img src="<?php echo base_url('uploads/personal/' . $personalEditar->fotografia); ?>" alt="Foto del empleado" width="200"><br>
              <?php else: ?>
                  <p>No hay foto</p>
              <?php endif; ?>
              <br>
               <label for=""><b>Nueva Foto:</b></label><br>

               <input type="file" name="fotografia_nueva" id="fotografia_nueva" accept="image/*" class="form-control">

          </div>

          <div class="row justify-content-end">
              <div class="col-auto">
                  <button type="submit" name="button" class="btn btn-success">
                      <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                  </button>
              </div>
              <div class="col-auto">
                  <a class="btn btn-danger" href="<?php echo site_url('personales/index') ?>">
                      <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                  </a>
              </div>
          </div>



        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('personales/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
  input.value = input.value.toUpperCase();

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>
<script>
  $(document).ready(function () {
    $("#fotografia_nueva").fileinput({
      //showUpload:false
      //showRemove: false,
      language:'es',
    });
  });
</script>


<script type="text/javascript">
$(document).ready(function() {
  $.validator.addMethod("fechaActual", function(value, element) {
    var fechaActual = new Date();
    var fechaInscripcion = new Date(value);
    return fechaInscripcion <= fechaActual;
  }, "La fecha de contratación no puede ser después de la fecha actual.");
    $("#frm_nuevo_personal").validate({
        rules: {
          "nombre": {
              required: true,
              minlength: 3,
              maxlength: 50

          },
          "dirección": {
              required: true,
              minlength: 3,
              maxlength: 150

          },
            "puesto": {
                required: true,
                minlength: 3,
                maxlength: 50

            },

            "telefono": {
              required: true,
              minlength: 7,
              maxlength: 10
            },
            "fecha_contratacion": {
              required: true,
              fechaActual: true

            },

            "estado": {
              required: true
            },
            "fotografia": {
              required: true
            }

        },
        messages: {
          "nombre": {
              required: "Ingrese el nombre del empleado",
              minlength: "El nombre del empleado no debe tener menos de 3 caracteres",
              maxlength: "El nombre del empleado no debe tener más de 50 caracteres"

          },
          "direccion": {
              required: "Ingrese la direccion",
              minlength: "La dirección no debe tener menos de 3 caracteres",
              maxlength: "La dirección no debe tener más de 150 caracteres"

          },
            "puesto": {
                required: "Ingrese el puesto",
                minlength: "El puesto no debe tener menos de 3 caracteres",
                maxlength: "El puesto no debe tener más de 50 caracteres"

            },
            "telefono": {
              required: "Debe ingresar el telefono",
              minlength: "El telefono no debe tener menos de 7 numeros",
              maxlength: "El teléfono no debe tener más de 10 numeros"


            },
            "fecha_contratacion": {
              required: "Debe ingresar la fecha de contración"
            },

            "estado": {
              required: "Debe escoger el estado del empleado"
            },

            "fotografia": {
              required: "Debe ingresar la foto del empleado"
            }


        }
    });
});


</script>



<style media="screen">
  input{
    color: black !important;
  }
</style>
