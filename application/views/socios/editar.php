<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR SOCIO
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('socios/actualizarSocio'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_socio">

        <input type="hidden" value="<?php echo $socioEditar->id; ?>" name="id" id="id">
        <div class="mb-3 text-dark">
            <label for="cedula" class="form-label text-dark"><b>Cédula:</b></label>
            <input id="cedula" type="text" name="cedula" value="<?php echo $socioEditar->cedula; ?>" oninput="validarNumeros(this)" placeholder="Ingrese la cédula del socio" class="form-control" required>
        </div>
          <div class="mb-3 text-dark">
              <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
              <input id="nombre" type="text" name="nombre" value="<?php echo $socioEditar->nombre; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre del socio" class="form-control" required>
          </div>
          <div class="mb-3 text-dark">
              <label for="direccion" class="form-label text-dark"><b>Dirección:</b></label>
              <input id="direccion" type="text" name="direccion" value="<?php echo $socioEditar->direccion; ?>" oninput="" placeholder="Ingrese la dirección" class="form-control" required>
          </div>
          <div class="mb-3">
              <label for="telefono" class="form-label"><b>Teléfono:</b></label>
              <input id="telefono" type="text" name="telefono" value="<?php echo $socioEditar->telefono; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control" required>
          </div>
          <div class="mb-3">
              <label for="correo" class="form-label"><b>Correo:</b></label>
              <input id="correo" type="email" name="correo" value="<?php echo $socioEditar->correo; ?>" oninput="" placeholder="Ingrese el correo electrónico" class="form-control" required>
          </div>
          <div class="mb-3">
              <label for="fecha_inscripcion" class="form-label"><b>Fecha de inscripción:</b></label>
              <input id="fecha_inscripcion" type="date" name="fecha_inscripcion" value="<?php echo $socioEditar->fecha_inscripcion; ?>" oninput="" placeholder="Ingrese la fecha de inscripción" class="form-control" required>
          </div>




        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('socios/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
  input.value = input.value.toUpperCase();

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>

<script type="text/javascript">
$(document).ready(function() {
  $.validator.addMethod("fechaActual", function(value, element) {
    var fechaActual = new Date();
    var fechaInscripcion = new Date(value);
    return fechaInscripcion <= fechaActual;
  }, "La fecha de inscripción no puede ser después de la fecha actual.");
    $("#frm_nuevo_socio").validate({
        rules: {
          "cedula": {
              required: true,
              minlength: 10,
              maxlength: 10

          },
          "nombre": {
              required: true,
              minlength: 3,
              maxlength: 150

          },
            "direccion": {
                required: true,
                minlength: 5,
                maxlength: 150

            },

            "telefono": {
              required: true,
              minlength: 7,
              maxlength: 10
            },
            "correo": {
              required: true,
              email: true
            },

            "fecha_inscripcion": {
              required: true,
              fechaActual: true


            }
        },
        messages: {
          "cedula": {
              required: "Ingrese la cédula del socio",
              minlength: "El nombre del libro no debe tener menos de 10 caracteres",
              maxlength: "El nombre del libro no debe tener más de 10 caracteres"

          },
          "nombre": {
              required: "Ingrese el nombre del socio",
              minlength: "El nombre no debe tener menos de 3 caracteres",
              maxlength: "El nombre del libro no debe tener más de 100 caracteres"

          },
            "direccion": {
                required: "Ingrese la direccion",
                minlength: "La dirección no debe tener menos de 5 caracteres",
                maxlength: "La dirección no debe tener más de 150 caracteres"

            },
            "telefono": {
              required: "Debe ingresar el telefono",
              minlength: "El telefono no debe tener menos de 7 numeros",
              maxlength: "El teléfono no debe tener más de 10 numeros"


            },
            "correo": {
              required: "Debe ingresar el correo electrónico",
              email: "Por favor, ingresa un correo electrónico válido"
            },

            "fecha_inscripcion": {
              required: "Debe ingresar la fecha de inscripción"
            }


        }
    });
});


</script>


<style media="screen">
  input{
    color: black !important;
  }
</style>
