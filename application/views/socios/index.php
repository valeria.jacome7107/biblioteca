<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
         <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;SOCIOS</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar nuevo Socio
      </button>


    </div>

  </div><br>


  <?php if ($listadoSocios): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>CEDULA</th>
            <th>NOMBRE</th>
            <th>DIRECCIÓN</th>
            <th>TELEFONO</th>
            <th>CORREO</th>
            <th>FECHA DE INSCRIPCIÓN</th>
            <th>ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoSocios as $socio): ?>
            <tr>
                <td class="text-dark"><?php echo $socio->id; ?></td>
                <td class="text-dark"><?php echo $socio->cedula; ?></td>
                <td class="text-dark"><?php echo $socio->nombre; ?></td>
                <td class="text-dark"><?php echo $socio->direccion; ?></td>
                <td class="text-dark"><?php echo $socio->telefono; ?></td>
                <td class="text-dark"><?php echo $socio->correo; ?></td>
                <td class="text-dark"><?php echo $socio->fecha_inscripcion; ?></td>
                <td>
                    <a href="<?php echo site_url('socios/editar/').$socio->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('socios/borrar/').$socio->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro socios registrados
          </div>
  <?php endif; ?>
</div>


      <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"> Nuevo Socio
                  </h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
      <div class="container">
          <form  class="text-dark" action="<?php echo site_url('socios/guardarSocio') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_socio">

            <div class="mb-3 text-dark">
                <label for="cedula" class="form-label text-dark"><b>Cédula:</b></label>
                <input id="cedula" type="text" name="cedula" value="" oninput="validarNumeros(this)" placeholder="Ingrese la cédula del socio" class="form-control" required>
            </div>
              <div class="mb-3 text-dark">
                  <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
                  <input id="nombre" type="text" name="nombre" value="" oninput="validarLetras(this)" placeholder="Ingrese el nombre del socio" class="form-control" required>
              </div>
              <div class="mb-3 text-dark">
                  <label for="direccion" class="form-label text-dark"><b>Dirección:</b></label>
                  <input id="direccion" type="text" name="direccion" value="" oninput="" placeholder="Ingrese la dirección" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="telefono" class="form-label"><b>Teléfono:</b></label>
                  <input id="telefono" type="text" name="telefono" value="" oninput="validarNumeros(this)" placeholder="Ingrese el teléfono" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="correo" class="form-label"><b>Correo:</b></label>
                  <input id="correo" type="email" name="correo" value="" oninput="" placeholder="Ingrese el correo electrónico" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="fecha_inscripcion" class="form-label"><b>Fecha de inscripción:</b></label>
                  <input id="fecha_inscripcion" type="date" name="fecha_inscripcion" value="" oninput="" placeholder="Ingrese la fecha de inscripción" class="form-control" required>
              </div>

              <div class="row justify-content-end">
                  <div class="col-auto">
                      <button type="submit" name="button" class="btn btn-success">
                          <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                      </button>
                  </div>
                  <div class="col-auto">
                      <a class="btn btn-danger" href="<?php echo site_url('socios/index') ?>">
                          <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                      </a>
                  </div>
              </div>
          </form>
      </div>
  </div>

  </div>
  </div>
    </div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>


      <script type="text/javascript">
      $(document).ready(function() {
        $.validator.addMethod("fechaActual", function(value, element) {
          var fechaActual = new Date();
          var fechaInscripcion = new Date(value);
          return fechaInscripcion <= fechaActual;
        }, "La fecha de inscripción no puede ser después de la fecha actual.");
          $("#frm_nuevo_socio").validate({
              rules: {
                "cedula": {
                    required: true,
                    minlength: 10,
                    maxlength: 10

                },
                "nombre": {
                    required: true,
                    minlength: 3,
                    maxlength: 150

                },
                  "direccion": {
                      required: true,
                      minlength: 5,
                      maxlength: 150

                  },

                  "telefono": {
                    required: true,
                    minlength: 7,
                    maxlength: 10
                  },
                  "correo": {
                    required: true,
                    email: true
                  },

                  "fecha_inscripcion": {
                    required: true,
                    fechaActual: true


                  }
              },
              messages: {
                "cedula": {
                    required: "Ingrese la cédula del socio",
                    minlength: "El nombre del libro no debe tener menos de 10 caracteres",
                    maxlength: "El nombre del libro no debe tener más de 10 caracteres"

                },
                "nombre": {
                    required: "Ingrese el nombre del socio",
                    minlength: "El nombre no debe tener menos de 3 caracteres",
                    maxlength: "El nombre del libro no debe tener más de 100 caracteres"

                },
                  "direccion": {
                      required: "Ingrese la direccion",
                      minlength: "La dirección no debe tener menos de 5 caracteres",
                      maxlength: "La dirección no debe tener más de 150 caracteres"

                  },
                  "telefono": {
                    required: "Debe ingresar el telefono",
                    minlength: "El telefono no debe tener menos de 7 numeros",
                    maxlength: "El teléfono no debe tener más de 10 numeros"


                  },
                  "correo": {
                    required: "Debe ingresar el correo electrónico",
                    email: "Por favor, ingresa un correo electrónico válido"
                  },

                  "fecha_inscripcion": {
                    required: "Debe ingresar la fecha de inscripción"
                  }


              }
          });
      });


      </script>

      <style media="screen">
        input{
          color: black !important;
        }
      </style>
