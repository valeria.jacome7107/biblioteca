<div style="padding: 150px 100px 20px 100px">
<h1>
<b>
  <i class="fa fa-plus-circle"></i>
  EDITAR MULTAS
</b>
</h1>
<br>

<form class="" action="<?php echo site_url('multas/actualizarMulta'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_multa">
  <div class="mb-3 text-dark">
    <label for="fkid_devolucion"><b>ID de Devoluciones:</b></label>
    <select name="fkid_devolucion" id="fkid_devolucion" class="form-control selectpicker" required>
<option value="">--Seleccione el ID  de devoluciones--</option>
<?php foreach ($listadoDevoluciones as $devolucion): ?>
   <option value="<?php echo $devolucion->id; ?>" <?php if ($devolucion->id == $multaEditar->fkid_devolucion) echo "selected"; ?>>
       <?php echo $devolucion->id; ?>
   </option>
<?php endforeach; ?>
</select>
  <br>
  </div>
	<input type="hidden" value="<?php echo $multaEditar->id; ?>" name="id" id="id">
  <div class="mb-3 text-dark">
      <label for="monto" class="form-label text-dark"><b>Monto:</b></label>
      <input id="monto" type="text" name="monto" value="<?php echo $multaEditar->monto; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el monto a pagar" class="form-control" required>
  </div>
  <div class="mb-3">
      <label for="pagado" class="form-label"><b>Pagado:</b></label>
      <select id="pagado" name="pagado" class="form-control" required>
          <option value="">Seleccione si el pago fue cancelado</option>
          <option value="Si" <?php if ($multaEditar->pagado == "Si") echo "selected"; ?>>Si</option>
          <option value="No" <?php if ($multaEditar->pagado == "No") echo "selected"; ?>>No</option>
      </select>
  </div>
    <div class="mb-3 text-dark">
        <label for="fecha_pago" class="form-label text-dark"><b>Fecha de pago:</b></label>
        <input id="fecha_pago" type="date" name="fecha_pago" value="<?php echo $multaEditar->fecha_pago; ?>" oninput="" placeholder="Ingrese la fecha de pago" class="form-control" required>
    </div>

<br>
<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
    &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="btn btn-danger" href=" <?php echo site_url('directores/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
  </div>

</div>

</form>

</div>
<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
  input.value = input.value.toUpperCase();

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>
<script>
      $(document).ready(function() {
        $('#fkid_edi').select2({
          placeholder: "--Seleccione la Editorial--", // Texto del placeholder
          allowClear: true // Opcional, para permitir que se limpie la selección
        });
      });
</script>
<script>
  $(document).ready(function () {
    $("#fotografia_nueva").fileinput({
      //showUpload:false
      //showRemove: false,
      language:'es',
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
$("#frm_nuevo_multa").validate({
    rules: {
        "fkid_devolucion": {
            required: true
        },
        "monto": {
            required: true,
            minlength: 1,
            maxlength: 4
        },
        "pagado": {
            required: true
        },
        "fecha_pago": {
            required: true,
            maxDateToday: true // Regla personalizada para validar la fecha máxima
        }
    },
    messages: {
        "fkid_devolucion": {
            required: "Escoja la ID de la devolución"
        },
        "monto": {
            required: "Ingrese el monto a pagar",
            minlength: "El monto debe tener más de 1 número",
            maxlength: "El monto debe tener menos de 4 números"
        },
        "pagado": {
            required: "Ingrese si la multa está pagada"
        },
        "fecha_pago": {
            required: "Debe ingresar la fecha de pago",
            maxDateToday: "La fecha de pago no puede ser posterior a la fecha actual"
        }
    }
});

// Regla personalizada para validar la fecha máxima
$.validator.addMethod("maxDateToday", function(value, element) {
    var fecha_actual = new Date();
    var fecha_pago = new Date(value);
    return fecha_pago <= fecha_actual;
}, "La fecha de pago no puede ser posterior a la fecha actual.");
});


</script>

<style media="screen">
  input{
    color: black !important;
  }
</style>
