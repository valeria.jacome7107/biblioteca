<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
         <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;MULTAS</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar nueva multa
      </button>


    </div>

  </div><br>


  <?php if ($listadoMultas): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>MONTO</th>
            <th>PAGADO</th>
            <th>FECHA DE PAGO</th>
            <th>ID DE DEVOLUCIONES</th>
            <th>ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoMultas as $multa): ?>
            <tr>
                <td class="text-dark"><?php echo $multa->id; ?></td>
                <td class="text-dark"><?php echo $multa->monto; ?></td>
                <td class="text-dark"><?php echo $multa->pagado; ?></td>
                <td class="text-dark"><?php echo $multa->fecha_pago; ?></td>
                <td class="text-dark"><?php echo $multa->fkid_devolucion; ?></td>
                <td>
                    <a href="<?php echo site_url('multas/editar/').$multa->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('multas/borrar/').$multa->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro multas registradas
          </div>
  <?php endif; ?>
</div>


      <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"> Nueva Multa
                  </h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
      <div class="container">
          <form  class="text-dark" action="<?php echo site_url('multas/guardarMulta') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_multa">
            <div class="mb-3 text-dark">
              <label for="fkid_edi"><b>ID devolución:</b></label>
                  <select name="fkid_devolucion" id="fkid_devolucion" class="form-control" required>
                      <option value="">--Seleccione la ID de la Devolución--</option>
                      <?php foreach ($listadoDevoluciones as $devolucion): ?>
                          <option value="<?php echo $devolucion->id; ?>"><?php echo $devolucion->id; ?></option>
                      <?php endforeach; ?>
                  </select>
            </div>

            <div class="mb-3 text-dark">
                <label for="monto" class="form-label text-dark"><b>Monto:</b></label>
                <input id="monto" type="text" name="monto" value="" oninput="validarNumeros(this)" placeholder="Ingrese el monto a pagar" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="pagado" class="form-label"><b>Pagado:</b></label>
                <select id="pagado" name="pagado" class="form-control" required>
                    <option value="">Seleccione si el pago fue cancelado</option>
                    <option value="Si">Si</option>
                    <option value="No">No</option>
                </select>
            </div>
              <div class="mb-3 text-dark">
                  <label for="fecha_pago" class="form-label text-dark"><b>Fecha de pago:</b></label>
                  <input id="fecha_pago" type="date" name="fecha_pago" value="" oninput="" placeholder="Ingrese la fecha de pago" class="form-control" required>
              </div>


              <div class="row justify-content-end">
                  <div class="col-auto">
                      <button type="submit" name="button" class="btn btn-success">
                          <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                      </button>
                  </div>
                  <div class="col-auto">
                      <a class="btn btn-danger" href="<?php echo site_url('multas/index') ?>">
                          <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                      </a>
                  </div>
              </div>
          </form>
      </div>
  </div>

  </div>
  </div>
    </div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>

      <script>
        $(document).ready(function () {
          $("#fotografia_nueva").fileinput({
            //showUpload:false
            //showRemove: false,
            language:'es',
          });
        });
      </script>

      <script type="text/javascript">
      $(document).ready(function() {
      $("#frm_nuevo_multa").validate({
          rules: {
              "fkid_devolucion": {
                  required: true
              },
              "monto": {
                  required: true,
                  minlength: 1,
                  maxlength: 4
              },
              "pagado": {
                  required: true
              },
              "fecha_pago": {
                  required: true,
                  maxDateToday: true // Regla personalizada para validar la fecha máxima
              }
          },
          messages: {
              "fkid_devolucion": {
                  required: "Escoja la ID de la devolución"
              },
              "monto": {
                  required: "Ingrese el monto a pagar",
                  minlength: "El monto debe tener más de 1 número",
                  maxlength: "El monto debe tener menos de 4 números"
              },
              "pagado": {
                  required: "Ingrese si la multa está pagada"
              },
              "fecha_pago": {
                  required: "Debe ingresar la fecha de pago",
                  maxDateToday: "La fecha de pago no puede ser posterior a la fecha actual"
              }
          }
      });

      // Regla personalizada para validar la fecha máxima
      $.validator.addMethod("maxDateToday", function(value, element) {
          var fecha_actual = new Date();
          var fecha_pago = new Date(value);
          return fecha_pago <= fecha_actual;
      }, "La fecha de pago no puede ser posterior a la fecha actual.");
  });

      </script>

      <style media="screen">
        input{
          color: black !important;
        }
      </style>
