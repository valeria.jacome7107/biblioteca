<!-- application/views/pdf_view.php -->
<!DOCTYPE html>
<html>
<head>
    <title>PDF Generado</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.68/vfs_fonts.js"></script>
</head>
<body>
    <script>
        // Obtiene el contenido del PDF desde PHP
        var pdfContent = <?php echo json_encode($pdfContent); ?>;

        // Define la función para generar el PDF
        function generatePDF() {
            var docDefinition = {
                content: pdfContent,
                styles: {
                    header: {
                        fontSize: 22,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                    subheader: {
                        fontSize: 18,
                        bold: true,
                        margin: [0, 10, 0, 5]
                    },
                    section: {
                        fontSize: 16,
                        bold: true,
                        margin: [0, 10, 0, 5]
                    }
                }
            };

            // Genera el PDF
            pdfMake.createPdf(docDefinition).open();
        }

        // Llama a la función para generar el PDF al cargar la página
        generatePDF();
    </script>
</body>
</html>
