<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR PRÉSTAMO
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('prestamos/actualizarPrestamo'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_prestamo">

        <input type="hidden" value="<?php echo $prestamoEditar->id; ?>" name="id" id="id">


        <label for="fkid_personal"><b>Personal:</b></label>
        <select name="fkid_personal" id="fkid_personal" class="form-control selectpicker" required>
   <option value="">--Seleccione el personal--</option>
   <?php foreach ($listadoPersonales as $personal): ?>
       <option value="<?php echo $personal->id; ?>" <?php if ($personal->id == $prestamoEditar->fkid_personal) echo "selected"; ?>>
           <?php echo $personal->nombre; ?>
       </option>
   <?php endforeach; ?>
</select>
      <br>

      <label for="fkid_libro"><b>Libro:</b></label>
  <select name="fkid_libro" id="fkid_libro" class="form-control selectpicker" data-live-search="true" data-live-search-style="startsWith" required>
      <option value="">--Seleccione el libro--</option>
      <?php foreach ($listadoLibros as $libro): ?>
          <option value="<?php echo $libro->id; ?>" <?php if ($libro->id == $prestamoEditar->fkid_libro) echo "selected"; ?>>
              <?php echo $libro->nombre; ?>
          </option>
      <?php endforeach; ?>
  </select>
    <br>
    <label for="fkid_socio"><b>Socio:</b></label>
<select name="fkid_socio" id="fkid_socio" class="form-control selectpicker" data-live-search="true" data-live-search-style="startsWith" required>
    <option value="">--Seleccione el socio--</option>
    <?php foreach ($listadoSocios as $socio): ?>
        <option value="<?php echo $socio->id; ?>" <?php if ($socio->id == $prestamoEditar->fkid_socio) echo "selected"; ?>>
            <?php echo $socio->nombre; ?>
        </option>
    <?php endforeach; ?>
</select>
    <br>
  <div class="mb-3">
      <label for="fecha_prestamo" class="form-label"><b>Fecha de Préstamo:</b></label>
      <input id="fecha_prestamo" type="date" name="fecha_prestamo" value="<?php echo $prestamoEditar->fecha_prestamo; ?>" oninput="" placeholder="Ingrese la fecha de préstamo" class="form-control" required>
  </div>
  <div class="mb-3">
      <label for="fecha_devolucion_prevista" class="form-label"><b>Fecha de devolución:</b></label>
      <input id="fecha_devolucion_prevista" type="date" name="fecha_devolucion_prevista" value="<?php echo $prestamoEditar->fecha_devolucion_prevista; ?>" oninput="" placeholder="Ingrese la fecha de devolución" class="form-control" required>
  </div>


        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('prestamos/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>


<script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>


      <script type="text/javascript">
      $(document).ready(function() {
        $.validator.addMethod("fechaActual", function(value, element) {
          var fechaActual = new Date();
          var fechaInscripcion = new Date(value);
          return fechaInscripcion <= fechaActual;
        }, "La fecha de préstamo no puede ser después de la fecha actual.");
          $("#frm_nuevo_prestamo").validate({
              rules: {
                "fkid_personal": {
                    required: true

                },
                  "fkid_libro": {
                      required: true

                  },
                  "fkid_socio": {
                      required: true

                  },
                  "fecha_prestamo": {
                      required: true,
                      fechaActual: true

                  },
                  "fecha_devolucion_prevista": {
                      required: true

                  }
              },
              messages: {
                "fkid_personal": {
                    required: "Escoja el personal"

                },
                  "fkid_libro": {
                      required: "Escoja el libro"

                  },
                  "fkid_socio": {
                      required: "Escoja el socio"

                  },
                  "fecha_prestamo": {
                      required: "Ingrese la fecha de préstamo"

                  },
                  "fecha_devolucion_prevista": {
                      required: "Ingrese la fecha de devolución prevista"

                  }




              }
          });
      });


      </script>

      <style media="screen">
        input{
          color: black !important;
        }
      </style>
