
<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
       <h1><i class="fa-solid fa-book-open"></i>&nbsp;&nbsp;PRÉSTAMOS</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar Nuevo Préstamo
      </button>


    </div>

  </div><br>


  <?php if ($listadoPrestamos): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>FECHA DE PRÉSTRAMO</th>
            <th>FECHA DE DEVOLUCIÓN</th>
            <th>PERSONAL</th>
            <th>LIBRO</th>
            <th>SOCIO</th>
            <th>ACCIONES</th>

        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoPrestamos as $prestamo): ?>
            <tr>
                <td class="text-dark"><?php echo $prestamo->id; ?></td>
                <td class="text-dark"><?php echo $prestamo->fecha_prestamo; ?></td>
                <td class="text-dark"><?php echo $prestamo->fecha_devolucion_prevista; ?></td>
                <td class="text-dark"><?php echo $prestamo->nombre_personal; ?></td>
                <td class="text-dark"><?php echo $prestamo->nombre_libro; ?></td>
                <td class="text-dark"><?php echo $prestamo->nombre_socio; ?></td>

                <td>
                    <a href="<?php echo site_url('prestamos/editar/').$prestamo->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('prestamos/borrar/').$prestamo->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                    <a href="<?php echo site_url('prestamos/generatePdf/'.$prestamo->id); ?>" class="btn btn-primary" title="Imprimir PDF" target="_blank">
                       <i class="fa-solid fa-file-pdf"></i>
                   </a>



                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>



  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro préstamos registradas
          </div>
  <?php endif; ?>
</div>

<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> Nuevo Préstamo
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
<div class="container">
    <form  class="text-dark"action="<?php echo site_url('prestamos/guardarPrestamo') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_prestamo">

      <div class="mb-3 text-dark">
    <label for="fkid_personal"><b>Personal:</b></label>
    <select name="fkid_personal" id="fkid_personal" class="form-control" required>
        <option value="">--Seleccione el personal--</option>
        <?php foreach ($listadoPersonales as $personal): ?>
            <option value="<?php echo $personal->id; ?>"><?php echo $personal->nombre; ?></option>
        <?php endforeach; ?>
    </select>
      </div>


        <div class="mb-3 text-dark">
      <label for="fkid_libro"><b>Libro:</b></label>
      <select name="fkid_libro" id="fkid_libro" class="form-control" required>
          <option value="">--Seleccione el libro--</option>
          <?php foreach ($listadoLibros as $libro): ?>
              <option value="<?php echo $libro->id; ?>"><?php echo $libro->nombre; ?> </option>
          <?php endforeach; ?>
      </select>
        </div>

        <div class="mb-3 text-dark">
      <label for="fkid_socio"><b>Socio:</b></label>
      <select name="fkid_socio" id="fkid_socio" class="form-control" required>
          <option value="">--Seleccione el socio--</option>
          <?php foreach ($listadoSocios as $socio): ?>
              <option value="<?php echo $socio->id; ?>"><?php echo $socio->nombre; ?> </option>
          <?php endforeach; ?>
      </select>
        </div>
        <div class="mb-3">
            <label for="fecha_prestamo" class="form-label"><b>Fecha de Préstamo:</b></label>
            <input id="fecha_prestamo" type="date" name="fecha_prestamo" value="" oninput="" placeholder="Ingrese la fecha de préstamo" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="fecha_devolucion_prevista" class="form-label"><b>Fecha de devolución:</b></label>
            <input id="fecha_devolucion_prevista" type="date" name="fecha_devolucion_prevista" value="" oninput="" placeholder="Ingrese la fecha de devolución" class="form-control" required>
        </div>



        <div class="row justify-content-end">
            <div class="col-auto">
                <button type="submit" name="button" class="btn btn-success">
                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                </button>
            </div>
            <div class="col-auto">
                <a class="btn btn-danger" href="<?php echo site_url('prestamos/index') ?>">
                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                </a>
            </div>
        </div>
    </form>
</div>
</div>

</div>

</div>
</div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>

      <script type="text/javascript">
      $(document).ready(function() {
        $.validator.addMethod("fechaActual", function(value, element) {
          var fechaActual = new Date();
          var fechaInscripcion = new Date(value);
          return fechaInscripcion <= fechaActual;
        }, "La fecha de préstamo no puede ser después de la fecha actual.");
          $("#frm_nuevo_prestamo").validate({
              rules: {
                "fkid_personal": {
                    required: true

                },
                  "fkid_libro": {
                      required: true

                  },
                  "fkid_socio": {
                      required: true

                  },
                  "fecha_prestamo": {
                      required: true,
                      fechaActual: true

                  },
                  "fecha_devolucion_prevista": {
                      required: true

                  }
              },
              messages: {
                "fkid_personal": {
                    required: "Escoja el personal"

                },
                  "fkid_libro": {
                      required: "Escoja el libro"

                  },
                  "fkid_socio": {
                      required: "Escoja el socio"

                  },
                  "fecha_prestamo": {
                      required: "Ingrese la fecha de préstamo"

                  },
                  "fecha_devolucion_prevista": {
                      required: "Ingrese la fecha de devolución prevista"

                  }




              }
          });
      });


      </script>
      <style media="screen">
        input{
          color: black !important;
        }
      </style>
