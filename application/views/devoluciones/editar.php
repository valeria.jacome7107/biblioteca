<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR DEVOLUCIÓN
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('devoluciones/actualizarDevolucion'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_devolucion">

        <input type="hidden" value="<?php echo $devolucionEditar->id; ?>" name="id" id="id">


        <label for="fkid_personal"><b>Personal:</b></label>
        <select name="fkid_personal" id="fkid_personal" class="form-control selectpicker" required>
   <option value="">--Seleccione el personal--</option>
   <?php foreach ($listadoPersonales as $personal): ?>
       <option value="<?php echo $personal->id; ?>" <?php if ($personal->id == $devolucionEditar->fkid_personal) echo "selected"; ?>>
           <?php echo $personal->nombre; ?>
       </option>
   <?php endforeach; ?>
</select>
      <br>

      <label for="fkid_prestamo"><b>Fecha de devolución prevista:</b></label>
  <select name="fkid_prestamo" id="fkid_prestamo" class="form-control selectpicker" data-live-search="true" data-live-search-style="startsWith" required>
      <option value="">--Seleccione la fecha--</option>
      <?php foreach ($listadoPrestamos as $prestamo): ?>
          <option value="<?php echo $prestamo->id; ?>" <?php if ($prestamo->id == $devolucionEditar->fkid_prestamo) echo "selected"; ?>>
              <?php echo $prestamo->fecha_devolucion_prevista; ?>
          </option>
      <?php endforeach; ?>
  </select>

    <br>
  <div class="mb-3">
      <label for="fecha_devolucion" class="form-label"><b>Fecha de Devolución:</b></label>
      <input id="fecha_devolucion" type="date" name="fecha_devolucion" value="<?php echo $devolucionEditar->fecha_devolucion; ?>" oninput="" placeholder="Ingrese la fecha de devolucion" class="form-control" required>
  </div>
  <div class="mb-3">
     <label for="estado" class="form-label"><b>Estado:</b></label>
     <select id="estado" name="estado" class="form-control" required>
         <option value="">Seleccione el estado</option>
         <option value="Entregado" <?php if ($devolucionEditar->estado == "Entregado") echo "selected"; ?>>Entregado</option>
         <option value="Con Retraso" <?php if ($devolucionEditar->estado == "Con Retraso") echo "selected"; ?>>Con Retraso</option>
     </select>
 </div>


        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('devoluciones/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>


<script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>


      <script type="text/javascript">
          $(document).ready(function() {
          $("#frm_nuevo_devolucion").validate({
              rules: {
                  "fkid_personal": {
                      required: true
                  },
                  "fkid_prestamo": {
                      required: true
                  },
                  "fecha_devolucion": {
                      required: true,
                      fechaActual: true // Nueva regla personalizada
                  },
                  "estado": {
                      required: true
                  }
              },
              messages: {
                  "fkid_personal": {
                      required: "Escoja el personal"
                  },
                  "fkid_prestamo": {
                      required: "Seleccione la fecha de préstamo"
                  },
                  "fecha_devolucion": {
                      required: "Ingrese la fecha de devolución"
                  },
                  "estado": {
                      required: "Escoja el estado de la devolución"
                  }
              }
          });

          // Agregar regla personalizada para validar que la fecha de devolución no sea posterior a la fecha actual
          $.validator.addMethod("fechaActual", function(value, element) {
              var fecha_actual = new Date();
              var fecha_devolucion = new Date(value);
              return fecha_devolucion <= fecha_actual;
          }, "La fecha de devolución no puede ser posterior a la fecha actual.");
          });


      </script>
      <style media="screen">
        input{
          color: black !important;
        }
      </style>
