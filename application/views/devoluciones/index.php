
<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
       <h1><i class="fa-solid fa-book-open"></i>&nbsp;&nbsp;DEVOLUCIONES</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar Nueva Devolución
      </button>


    </div>

  </div><br>


  <?php if ($listadoDevoluciones): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>FECHA DE DEVOLUCIÓN ACTUAL</th>
            <th>ESTADO</th>
            <th>PERSONAL</th>
            <th>FECHA DE DEVOLUCIÓN PREVISTA</th>
            <th>ACCIONES</th>

        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoDevoluciones as $devolucion): ?>
            <tr>
                <td class="text-dark"><?php echo $devolucion->id; ?></td>
                <td class="text-dark"><?php echo $devolucion->fecha_devolucion; ?></td>
                <td class="text-dark"><?php echo $devolucion->estado; ?></td>
                <td class="text-dark"><?php echo $devolucion->nombre_personal; ?></td>
                <td class="text-dark"><?php echo $devolucion->fecha_entrega; ?></td>

                <td>
                    <a href="<?php echo site_url('devoluciones/editar/').$devolucion->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('devoluciones/borrar/').$devolucion->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>



                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>



  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro devoluciones registradas
          </div>
  <?php endif; ?>
</div>

<!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"> Nueva Devolución
            </h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <div class="modal-body">
<div class="container">
    <form  class="text-dark"action="<?php echo site_url('devoluciones/guardarDevolucion') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_devolucion">

      <div class="mb-3 text-dark">
    <label for="fkid_personal"><b>Personal:</b></label>
    <select name="fkid_personal" id="fkid_personal" class="form-control" required>
        <option value="">--Seleccione el personal--</option>
        <?php foreach ($listadoPersonales as $personal): ?>
            <option value="<?php echo $personal->id; ?>"><?php echo $personal->nombre; ?></option>
        <?php endforeach; ?>
    </select>
      </div>


        <div class="mb-3 text-dark">
      <label for="fkid_prestamo"><b> Fecha de Devolución Prevista :</b></label>
      <select name="fkid_prestamo" id="fkid_prestamo" class="form-control" required>
          <option value="">--Seleccione la fecha del préstamo--</option>
          <?php foreach ($listadoPrestamos as $prestamo): ?>
              <option value="<?php echo $prestamo->id; ?>"><?php echo $prestamo->fecha_devolucion_prevista; ?> </option>
          <?php endforeach; ?>
      </select>
        </div>

        <div class="mb-3">
            <label for="fecha_devolucion" class="form-label"><b>Fecha de Devolución Actual:</b></label>
            <input id="fecha_devolucion" type="date" name="fecha_devolucion" value="" oninput="" placeholder="Ingrese la fecha de devolucion" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="estado" class="form-label"><b>Estado:</b></label>
            <select id="estado" name="estado" class="form-control" required>
                <option value="">Seleccione el estado</option>
                <option value="Entregado">Entregado</option>
                <option value="Con Retraso">Con Retraso</option>
            </select>
        </div>


        <div class="row justify-content-end">
            <div class="col-auto">
                <button type="submit" name="button" class="btn btn-success">
                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                </button>
            </div>
            <div class="col-auto">
                <a class="btn btn-danger" href="<?php echo site_url('devoluciones/index') ?>">
                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                </a>
            </div>
        </div>
    </form>
</div>
</div>

</div>

</div>
</div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>

      <script type="text/javascript">
      $.validator.addMethod("fechaNoPosterior", function(value, element) {
          var fecha_actual = new Date();
          var fecha_devolucion = new Date(value);
          return fecha_devolucion <= fecha_actual;
          }, "La fecha de devolución no puede ser posterior a la fecha actual.");

          $("#frm_nuevo_devolucion").validate({
          rules: {
           "fkid_personal": {
               required: true
           },
           "fkid_prestamo": {
               required: true
           },
           "fecha_devolucion": {
               required: true,
               fechaNoPosterior: true
           },
           "estado": {
               required: true
           }
          },
          messages: {
           "fkid_personal": {
               required: "Escoja el personal"
           },
           "fkid_prestamo": {
               required: "Seleccione la fecha de préstamo"
           },
           "fecha_devolucion": {
               required: "Ingrese la fecha de devolución",
               fechaNoPosterior: "La fecha de devolución no puede ser posterior a la fecha actual"
           },
           "estado": {
               required: "Escoja el estado de la devolución"
           }
          }
          });



      </script>
      <style media="screen">
        input{
          color: black !important;
        }
      </style>
