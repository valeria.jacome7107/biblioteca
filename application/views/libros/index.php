<div style="padding: 150px 70px 20px 100px">
     <div class="text-center">
         <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;LIBROS</h1>
    </div>
    <div class="row">
    <div class="col-md-12 text-end">

      <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
         <i class="fa fa-plus-circle fa-1x"></i> Agregar nuevo libro
      </button>


    </div>

  </div><br>


  <?php if ($listadoLibros): ?>
    <table class="table table-striped text-center">
    <thead class="table-dark">
        <tr>
            <th>ID</th>
            <th>NOMBRE</th>
            <th>AUTOR</th>
            <th>ISBN</th>
            <th>EDITORIAL</th>
            <th>IDIOMA</th>
            <th>NUMERO DE EJEMPLARES</th>
            <th>ACCIONES</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($listadoLibros as $libro): ?>
            <tr>
                <td class="text-dark"><?php echo $libro->id; ?></td>
                <td class="text-dark"><?php echo $libro->nombre; ?></td>
                <td class="text-dark"><?php echo $libro->autor; ?></td>
                <td class="text-dark"><?php echo $libro->isbn; ?></td>
                <td class="text-dark"><?php echo $libro->editorial; ?></td>
                <td class="text-dark"><?php echo $libro->idioma; ?></td>
                <td class="text-dark"><?php echo $libro->num_ejemplares; ?></td>
                <td>
                    <a href="<?php echo site_url('libros/editar/').$libro->id; ?>" class="btn btn-warning" title="Editar">
                        <i class="fa fa-pen"></i>
                    </a>
                    <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('libros/borrar/').$libro->id; ?>')">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>


  <div class="modal-footer">

  </div>

              </div>
            </div>
          </div>
    <?php else: ?>
          <div class="alert alert-danger">
              No se encontro libros registrados
          </div>
  <?php endif; ?>
</div>


      <!-- Modal -->
          <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel"> Nuevo Libro
                  </h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
      <div class="container">
          <form  class="text-dark" action="<?php echo site_url('libros/guardarLibro') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_libro">

            <div class="mb-3 text-dark">
                <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
                <input id="nombre" type="text" name="nombre" value="" oninput="validarLetras(this)" placeholder="Ingrese el nombre del libro" class="form-control" required>
            </div>
              <div class="mb-3 text-dark">
                  <label for="autor" class="form-label text-dark"><b>Autor:</b></label>
                  <input id="autor" type="text" name="autor" value="" oninput="validarLetras(this)" placeholder="Ingrese el nombre del autor" class="form-control" required>
              </div>
              <div class="mb-3 text-dark">
                  <label for="isbn" class="form-label text-dark"><b>ISBN:</b></label>
                  <input id="isbn" type="text" name="isbn" value="" oninput="" placeholder="Ingrese el ISBN" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="editorial" class="form-label"><b>Editorial:</b></label>
                  <input id="editorial" type="text" name="editorial" value="" oninput="" placeholder="Ingrese la editorial" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="idioma" class="form-label"><b>Idioma:</b></label>
                  <input id="idioma" type="text" name="idioma" value="" oninput="validarLetras(this)" placeholder="Ingrese el idioma del libro" class="form-control" required>
              </div>
              <div class="mb-3">
                  <label for="num_ejemplares" class="form-label"><b>Número de Ejemplares:</b></label>
                  <input id="num_ejemplares" type="text" name="num_ejemplares" value="" oninput="validarNumeros(this)" placeholder="Ingrese el numero de ejemplares" class="form-control" required>
              </div>

              <div class="row justify-content-end">
                  <div class="col-auto">
                      <button type="submit" name="button" class="btn btn-success">
                          <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                      </button>
                  </div>
                  <div class="col-auto">
                      <a class="btn btn-danger" href="<?php echo site_url('libros/index') ?>">
                          <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                      </a>
                  </div>
              </div>
          </form>
      </div>
  </div>

  </div>
  </div>
    </div>
<script>
function eliminarRegistro(url) {
              Swal.fire({
                  title: '¿Estas seguro de eliminar este registro?',
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: '¡Sí, eliminalo!',
                  cancelButtonText: 'Cancelar'
              }).then((result) => {
                  if (result.isConfirmed) {
                      // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
                      window.location.href = url;
                  } else {
                      // Si el usuario cancela, mostramos un mensaje de cancelación
                      Swal.fire(
                          'Cancelado',
                          'Tu registro no ha sido eliminado :P',
                          'error'
                      );
                  }
              });
          }
      </script>

      <script type="text/javascript">
      function validarLetras(input) {
        input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
        input.value = input.value.toUpperCase();

      }


      function validarNumeros(input) {
      input.value = input.value.replace(/\D/g, '');
      }

      </script>


      <script type="text/javascript">
      $(document).ready(function() {
          $("#frm_nuevo_libro").validate({
              rules: {
                "nombre": {
                    required: true,
                    minlength: 3,
                    maxlength: 150

                },
                "autor": {
                    required: true,
                    minlength: 3,
                    maxlength: 150

                },
                  "isbn": {
                      required: true,
                      minlength: 10,
                      maxlength: 17

                  },

                  "editorial": {
                    required: true,
                    minlength: 5,
                    maxlength: 50
                  },
                  "idioma": {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                  },

                  "num_ejemplares": {
                    required: true,
                    minlength: 1,
                    maxlength: 4


                  }
              },
              messages: {
                "nombre": {
                    required: "Ingrese el nombre del libro",
                    minlength: "El nombre del libro no debe tener menos de 3 caracteres",
                    maxlength: "El nombre del libro no debe tener más de 100 caracteres"

                },
                "autor": {
                    required: "Ingrese el autor del libro",
                    minlength: "El autor del libro no debe tener menos de 3 caracteres",
                    maxlength: "El autor del libro no debe tener más de 100 caracteres"

                },
                  "isbn": {
                      required: "Ingrese el ISBN",
                      minlength: "El isbn del libro no debe tener menos de 10 caracteres",
                      maxlength: "El isbn del libro no debe tener más de 17 caracteres"

                  },
                  "editorial": {
                    required: "Debe ingresar la editorial del libro",
                    minlength: "La editorial del libro no debe tener menos de 5 caracteres",
                    maxlength: "La editorial del libro no debe tener más de 50 caracteres"


                  },
                  "idioma": {
                    required: "Debe ingresar el idioma",
                    minlength: "El idioma del libro no debe tener menos de 5 caracteres",
                    maxlength: "El idioma del libro no debe tener más de 50 caracteres"
                  },

                  "num_ejemplares": {
                    required: "Debe ingresar el numero de ejemplares",
                    minlength: "El numero de ejemplares no debe tener menos de 1 caracteres",
                    maxlength: "El numero de ejemplares no debe tener más de 3 caracteres"
                  }


              }
          });
      });


      </script>
      <style media="screen">
        input{
          color: black !important;
        }
      </style>
