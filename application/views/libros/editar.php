<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR LIBRO
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('libros/actualizarLibro'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_libro">

        <input type="hidden" value="<?php echo $libroEditar->id; ?>" name="id" id="id">
        <div class="mb-3 text-dark">
            <label for="nombre" class="form-label text-dark"><b>Nombre:</b></label>
            <input id="nombre" type="text" name="nombre" value="<?php echo $libroEditar->nombre; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre del libro" class="form-control" required>
        </div>
          <div class="mb-3 text-dark">
              <label for="autor" class="form-label text-dark"><b>Autor:</b></label>
              <input id="autor" type="text" name="autor" value="<?php echo $libroEditar->autor ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre del autor" class="form-control" required>
          </div>
          <div class="mb-3 text-dark">
              <label for="isbn" class="form-label text-dark"><b>ISBN:</b></label>
              <input id="isbn" type="text" name="isbn" value="<?php echo $libroEditar->isbn; ?>" oninput="" placeholder="Ingrese el ISBN" class="form-control" required>
          </div>
          <div class="mb-3">
              <label for="editorial" class="form-label"><b>Editorial:</b></label>
              <input id="editorial" type="text" name="editorial" value="<?php echo $libroEditar->editorial; ?>" oninput="" placeholder="Ingrese la editorial" class="form-control" required>
          </div>
          <div class="mb-3">
              <label for="idioma" class="form-label"><b>Idioma:</b></label>
              <input id="idioma" type="text" name="idioma" value="<?php echo $libroEditar->idioma; ?>" oninput="validarLetras(this)" placeholder="Ingrese el idioma del libro" class="form-control" required>
          </div>
          <div class="mb-3">
              <label for="num_ejemplares" class="form-label"><b>Número de Ejemplares:</b></label>
              <input id="num_ejemplares" type="text" name="num_ejemplares" value="<?php echo $libroEditar->num_ejemplares; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el numero de ejemplares" class="form-control" required>
          </div>



        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('libros/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');
  input.value = input.value.toUpperCase();

}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>

<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_libro").validate({
        rules: {
          "nombre": {
              required: true,
              minlength: 3,
              maxlength: 150

          },
          "autor": {
              required: true,
              minlength: 3,
              maxlength: 150

          },
            "isbn": {
                required: true,
                minlength: 10,
                maxlength: 17

            },

            "editorial": {
              required: true,
              minlength: 5,
              maxlength: 50
            },
            "idioma": {
              required: true,
              minlength: 3,
              maxlength: 50
            },

            "num_ejemplares": {
              required: true,
              minlength: 1,
              maxlength: 4


            }
        },
        messages: {
          "nombre": {
              required: "Ingrese el nombre del libro",
              minlength: "El nombre del libro no debe tener menos de 3 caracteres",
              maxlength: "El nombre del libro no debe tener más de 100 caracteres"

          },
          "autor": {
              required: "Ingrese el autor del libro",
              minlength: "El autor del libro no debe tener menos de 3 caracteres",
              maxlength: "El autor del libro no debe tener más de 100 caracteres"

          },
            "isbn": {
                required: "Ingrese el ISBN",
                minlength: "El isbn del libro no debe tener menos de 10 caracteres",
                maxlength: "El isbn del libro no debe tener más de 17 caracteres"

            },
            "editorial": {
              required: "Debe ingresar la editorial del libro",
              minlength: "La editorial del libro no debe tener menos de 5 caracteres",
              maxlength: "La editorial del libro no debe tener más de 50 caracteres"


            },
            "idioma": {
              required: "Debe ingresar el idioma",
              minlength: "El idioma del libro no debe tener menos de 5 caracteres",
              maxlength: "El idioma del libro no debe tener más de 50 caracteres"
            },

            "num_ejemplares": {
              required: "Debe ingresar el numero de ejemplares",
              minlength: "El numero de ejemplares no debe tener menos de 1 caracteres",
              maxlength: "El numero de ejemplares no debe tener más de 3 caracteres"
            }


        }
    });
});


</script>
<style media="screen">
  input{
    color: black !important;
  }
</style>
